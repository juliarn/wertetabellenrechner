package de.panamo.wertetabellenrechner.setup;



public abstract class SetupStage<ResultType> {
    private String message;
    private String inputTypeDescriptor;

    public SetupStage(String message, String inputTypeDescriptor) {
        this.message = message;
        this.inputTypeDescriptor = inputTypeDescriptor;
    }

    public abstract ResultType handleInput(String input);

    String getMessage() {
        return this.message;
    }

    String getInputTypeDescriptor() {
        return inputTypeDescriptor;
    }

}
