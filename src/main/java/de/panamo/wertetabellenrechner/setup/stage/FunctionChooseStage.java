package de.panamo.wertetabellenrechner.setup.stage;


import de.panamo.wertetabellenrechner.function.LinearFunction;
import de.panamo.wertetabellenrechner.function.MathFunction;
import de.panamo.wertetabellenrechner.function.ParabolaFunction;
import de.panamo.wertetabellenrechner.setup.SetupStage;

public class FunctionChooseStage extends SetupStage<MathFunction> {


    public FunctionChooseStage() {
        super("Gebe an, von welcher Funktionsart du eine Wertetabelle erhalten willst.", "'linear' oder 'parabel'");
    }

    @Override
    public MathFunction handleInput(String input) {
        if(input.equalsIgnoreCase("linear"))
            return new LinearFunction();
        else if(input.equalsIgnoreCase("parabel"))
            return new ParabolaFunction();
        else {
            System.out.println("Ungültige Eingabe");
            return null;
        }
    }
}
