package de.panamo.wertetabellenrechner.setup.stage;


import de.panamo.wertetabellenrechner.setup.SetupStage;

public class TableDistanceStage extends SetupStage<Double> {


    public TableDistanceStage() {
        super("Gebe an, welchen Abstand die x-Werte der Tabelle haben sollen.", "Eine Zahl");
    }

    @Override
    public Double handleInput(String input) {
        try {
            return Double.valueOf(input);
        } catch (NumberFormatException exception) {
            System.out.println("Bitte gebe eine Zahl an");
            return null;
        }
    }
}
