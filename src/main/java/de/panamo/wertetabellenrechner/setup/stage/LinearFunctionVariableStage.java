package de.panamo.wertetabellenrechner.setup.stage;


import de.panamo.wertetabellenrechner.setup.SetupStage;

import java.util.ArrayList;
import java.util.List;

public class LinearFunctionVariableStage extends SetupStage<List<Double>> {


    public LinearFunctionVariableStage() {
        super("Gebe die Parameter der linearen Funktion m und b an.", "Zwei Zahlen durch ein Leerzeichen getrennt (z.B. -2 3)");
    }

    @Override
    public List<Double> handleInput(String input) {
        String[] rawNumbers = input.split(" ");
        if(rawNumbers.length == 2) {
            try {
                List<Double> vars = new ArrayList<>();
                vars.add(Double.valueOf(rawNumbers[0]));
                vars.add(Double.valueOf(rawNumbers[1]));
                return vars;
            } catch (NumberFormatException exception) {
                System.out.println("Bitte gebe zwei Zahlen an");
            }
        } else
            System.out.println("Bitte gebe zwei Zahlen an");
        return null;
    }
}
