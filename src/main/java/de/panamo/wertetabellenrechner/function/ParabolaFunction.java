package de.panamo.wertetabellenrechner.function;


import java.util.List;

public class ParabolaFunction implements MathFunction {
    private double a, b, c;

    @Override
    public double invoke(double x) {
        return (this.a * Math.pow(x, 2)) + (this.b * x) + this.c;
    }

    @Override
    public void insertVariables(List<Double> variables) {
        this.a = variables.get(0);
        this.b = variables.get(1);
        this.c = variables.get(2);
    }

}
